const express = require('express')
const app = express()
const router = require('./router')
const cors = require('cors');
const bodyParser = require('body-parser')

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use('/uploads/', express.static('./uploads/'))
app.use('/assets/', express.static('./assets/'))

app.use(cors())
  
app.use(router)

app.use(function (err, req, res, next) {
    return	res.status(500).json({
		err_code: 500,
		message: err.message
	})
})

app.listen(8081,()=>{
    console.log('App running at port 8081 ...')
})