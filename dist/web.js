const express = require('express')
const app = express()
const fs = require('fs')

app.use('/static/', express.static('./static/'))
app.get("/*", function (req, res, next) {
  fs.readFile("index.html",'utf-8', function (err, data) {
    if (err) {
      next(err); // Pass errors to Express.
    }
    else {
      res.send(data);
    }
  });
});
app.use(function (err, req, res, next) {
    return	res.status(500).json({
		err_code: 500,
		message: err.message
	})
})


app.listen(80,()=>{
    console.log('App running at port 80 ...')
})

