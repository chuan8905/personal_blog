const express = require('express')
const router = express.Router()
const fs = require('fs')
const path = require('path')
const User = require('../models/user')
// const Tag = require('../models/tag')
const Saying = require('../models/saying')
const FriendlyLink = require('../models/friendlink')
const Article = require('../models/article')
const Ip = require('../models/ip')
const Comment = require('../models/comment')
const Link = require('../models/link')
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const md5 = require('blueimp-md5')
const jsonParser = bodyParser.json()
const moment = require('moment');
const multer = require('multer');
const qiniu = require("qiniu");



//注册
router.post('/api/manage/register',function(req,res,next){
	var body = req.body
	var validate =body.name && body.email && body.password && body.password.length>0 && (body.email.length>0 && body.name.length<10)
	body.password = md5(md5(body.password))//加密密码
	var pattern = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	//验证数据
	if(validate && pattern.test(body.email)){
		//查询该用户是否被注册
		User.findOne({email:body.email},{email:1,_id:0},function(err,data){
			if (err){
				return res.status(500).json({
					success:false,
					message:'服务器错误'
				})
			}
			//该用户名已经被注册
			if (data){
				return res.status(200).json({
					err_code:2,
					message:'email  aleady exists.'
				})
			}
			//注册成功，保存该用户
			if(!data){
				if(body.email == 'lsc8905@gmail.com'){
					body.isAdmin = 'true'
				}else{
					if(body.isAdmin){
						delete body['isAdmin']
					}
				}
				body.created_time = formTime();
				new User(body).save(function (err,user,next) {
					if(err) {
						return next(err)
					}
				  return res.status(200).json({
						err_code:0,
						message:'register success'
					})
				})
			}
		})
		
	}else{ //表单数据错误
		return res.status(200).json({
			err_code:1,
			message:'register fail'
		})
	}
})


//登陆业务逻辑
router.post('/api/manage/login',function(req,res,next){
	var body = req.body
	if(body.email.length > 0  && body.password.length > 0){
		User.findOne({email:body.email,password:md5(md5(body.password))},{_id:0,password:0,created_time:0},function(err,data){
			if (err){
				return res.status(500).json({
					success:false,
					message:'服务器错误'
				})
			}
			//登陆成功
			if (data){
				let content ={email:body.email}; // 要生成token的主题信息
				let secretOrPrivateKey="happychuan";// 这是加密的key（密钥）
				let token = jwt.sign(content, secretOrPrivateKey, {
					expiresIn: 60*60*1  // 1小时过期
				});
				return res.status(200).json({
					err_code:0,
					message:'login success',
					token:token,
					data:{name:data.name,isAdmin:data.isAdmin,avatar:data.avatar}
				})
			}
		})
		//查询登陆账号是否存在，以及密码是否正确
		User.findOne({email:body.email},function(err,data){
			if (err){
				return res.status(500).json({
					success:false,
					message:'服务器错误'
				})
			}
			//检测账号是否存在，或者密码是否正确
			if(!data){
				return res.status(200).json({
					err_code:2,
					message:'email is not exit'
			})}
			if (data){
				if(data.password != md5(md5(body.password))){
					return res.status(200).json({
						err_code:1,
						message:'password error'
					})
				}			

			}
		})
	}else{
		return res.status(200).json({
			err_code:3,
			message:'login fail'
		})
	}
	
})

//主页数据逻辑
router.post('/api/manage/home',function(req,res){
	var body = req.body;
	//登陆状态校验
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
    jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
        if (err) {  //  时间失效的时候 || 伪造的token
            res.status(200).json({
				err_code:0,
				message:'token fail'
			})
        } else {
            res.status(200).json({
				err_code:1,
				message:'ok',
				data:decode.email
			})
        }
    })
})

//用户数据
router.post('/api/manage/user/get',function(req,res){
	var body = req.body;
	//登陆状态校验
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
    jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
        if (err) {  //  时间失效的时候 || 伪造的token
            res.status(200).json({
				err_code:0,
				message:'token fail'
			})
        } else {
			//用户总数
			User.estimatedDocumentCount({},function (err,data) {
				if(err) {
					return next(err)
				}
				if(!data){
					return res.status(200).json({
						err_code:1,
						message:'no user !'
					})
				}
				let total =data
				User.find({},{_id:0,password:0,avatar:0})
					.skip(body.page*body.limit)
					.limit(body.limit)
					.sort({'_id':-1})
					.exec(function (err,data) {
						if(err){
							next(err)
						}
						res.status(200).json({
							err_code:2,
							message:'ok',
							data:data,
							total:total
						})
					})
			})//total
        }//else
    })//jwt
})

//用户权限变更
router.post('/api/manage/privilege',function(req,res){
	var body = req.body;
	//登陆状态校验
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
    jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
        if (err) {  //  时间失效的时候 || 伪造的token
            res.status(200).json({
				err_code:0,
				message:'token fail'
			})
        } else {
			User.findOne({email:decode.email},{_id:0,password:0,created_time:0},function(err,data){
				if(err){
					next(err)
				}
				//查不到自己的身份，你完了
				if(!data){
					res.status(200).json({
						err_code:1,
						message:'you disappear !'
					})
				}else{
					//权限不足
					if(!data.isAdmin){
						res.status(200).json({
							err_code:2,
							message:'permission denied',
						})
					}else{
						//查询该用户的权限
						User.findOne({email:body.email},{_id:0,password:0,created_time:0},function(err,data){
							if(err){
								next(err)
							}
							//查不到要修改的用户的身份
							if(!data){
								res.status(200).json({
									err_code:3,
									message:'user disappear !'
								})
							}else{
								//超级管理员
								if(body.email == 'lsc8905@gmail.com'){
									res.status(200).json({
										err_code:5,
										message:'you are super admin , don`t can'
									})
									return ;
								}
								let isAdmin = !data.isAdmin
								//进行权限修改
								User.updateOne({email:body.email},{isAdmin:isAdmin},{multi:true},function (err,raw) {
									if(err) {
										return next(err)
									}
									if(raw) {
										res.status(200).json({
											err_code:4,
											message:'privilege update success'
										})
									}
								})
							}
						})
					}
				}
			})
        }
    })
})
//密码修改
router.post('/api/manage/updatepsw',function(req,res){
	let body = req.body
	if(body.password.length < 1){
		return res.status(200).json({
			err_code:4,
			message:'params is worning'
		})
	}
	let isLogin = checkLoginState(req,res)
	if(isLogin.code == 1){
		User.updateOne({email:isLogin.data.email},{password:md5(md5(body.password))},{multi:true},(err,data)=>{
			if(err){
				next(err)
			}
			if(data){
				res.status(200).json({
					err_code:1,
					message:'ok'
				})
			}
		})
	}
})

//名称修改
router.post('/api/manage/updatename',function(req,res){
	let body = req.body
	if(body.name.length < 1 || body.name.length > 10){
		return res.status(200).json({
			err_code:4,
			message:'params is worning'
		})
	}
	let isLogin = checkLoginState(req,res)
	if(isLogin.code == 1){
		User.updateOne({email:isLogin.data.email},{name:body.name},{multi:true},(err,data)=>{
			if(err){
				next(err)
			}
			if(data){
				res.status(200).json({
					err_code:1,
					message:'ok'
				})
			}
		})
	}
})
//用户头像修改
const storage = multer.diskStorage({
	// 用来配置文件上传的位置
	destination: (req, file, cb) => {
	  // 调用 cb 即可实现上传位置的配置
	  cb(null, './assets/avatar')
	},
	// 用来配置上传文件的名称（包含后缀）
	filename: (req, file, cb) => {
	  //filename 用于确定文件夹中的文件名的确定。 如果没有设置 filename，每个文件将设置为一个随机文件名，并且是没有扩展名的。
	  // 获取文件的后缀
	  // 拼凑文件名
	  cb(null,Date.now() + '.png')
	}
  })
const upload = multer({storage: storage})
router.post('/api/manage/avatar/update',upload.single('avatar'),function(req,res){
	let isLogin = checkLoginState(req,res)
	let removeUrl = './' + req.file.path.replace(/\\/g,'/')
	if(isLogin.code == 0){
		fs.unlink(removeUrl,err=>{
			if(err){
				next(err)
			}
			return res.status(200).json({
				err_code:0,
				message:'token fail'
			})
		})
		return false;
	}else{
		User.findOne({email:isLogin.data.email},function(err,data){
			if(err){
				next(err)
			}
			if(data){
				let avatarUrl = '.' + data.avatar
				if(avatarUrl != './assets/default-avatar.png'){
					fs.unlink(avatarUrl,err=>{
						if(err){
							next(err)
						}
					})
				}
			}
			User.updateOne({email:isLogin.data.email},{avatar:removeUrl.slice(1)},{multi:true},function(err,data){
				if(err){
					next(err)
				}
				if(data){
					res.status(200).json({
						err_code:1,
						message:'avatar update success',
						data:removeUrl
					})
				}
			})
		})
	}
})

//用户删除
router.post('/api/manage/user/del',function(req,res){
	var body = req.body;
	//登陆状态校验
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
    jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
        if (err) {  //  时间失效的时候 || 伪造的token
            res.status(200).json({
				err_code:0,
				message:'token fail'
			})
        } else {
			User.findOne({email:decode.email},function(err,data){
				if(err){
					next(err)
				}
				//查不到自己的身份，你完了
				if(!data){
					res.status(200).json({
						err_code:1,
						message:'you disappear !'
					})
				}else{
					//权限不足
					if(!data.isAdmin){
						res.status(200).json({
							err_code:2,
							message:'permission denied',
						})
					}else{
						//超级管理员
						if(body.email == 'lsc8905@gmail.com'){
							res.status(200).json({
								err_code:5,
								message:'you are super admin , don`t can'
							})
							return ;
						}
						//删除用户
						User.findOne({email:body.email},{avatar:1},function(err,data){
							if(err) {
								return next(err)
							}
							if(data.avatar != '/assets/default-avatar.png'){
								let removeUrl = './' + data.avatar
								fs.unlink(removeUrl,err=>{
									if(err){
										next(err)
									}
								})
								//删除头像，删除数据库中的用户
								User.deleteOne({email:body.email},function (err,raw) {
									if(err) {
										return next(err)
									}
									if(raw) {
										res.status(200).json({
											err_code:3,
											message:'OK'
										})
									}
									if(!raw) {
										res.status(200).json({
											err_code:4,
											message:'fail'
										})
									}
								})//delUser
							}else{
							//不用删除头像，删除数据库中的用户
							User.deleteOne({email:body.email},function (err,raw) {
								if(err) {
									return next(err)
								}
								if(raw) {
									res.status(200).json({
										err_code:3,
										message:'OK'
									})
								}
								if(!raw) {
									res.status(200).json({
										err_code:4,
										message:'fail'
									})
								}
							})//delUser
						}//else
						})
					}
				}
			})
        }
    })
})
//标签数据返回
router.get('/api/manage/tag/get',function(req,res){
	Article.find({check:1},{tag:1,_id:0}).distinct('tag').exec(function(err,data){
		if(err){
			next(err)
		}
		if(data.length == 0){
			res.status(200).json({
				err_code:0,
				message:'no tag'
			})
		}else{
			res.status(200).json({
				err_code:1,
				data:data
			})
		}
	})
})

/****************************语录*************************************** */
//语录保存
router.post('/api/manage/saying/save',function(req,res){
	const body = req.body
	if(body.saying == ''){
		return res.status(200).json({
			err_code:4,
			message:'fail'
		})
	}
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
    jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
        if (err) {  //  时间失效的时候 || 伪造的token
            res.status(200).json({
				err_code:0,
				message:'token fail'
			})
        } else {
			User.find({email:decode.email},function(err,data){
				if(err){
					next(err)
				}
				//查不到自己的身份，你完了
				if(!data){
					res.status(200).json({
						err_code:1,
						message:'you disappear !'
					})
				}else{
					//权限不足
					if(!data[0].isAdmin){
						res.status(200).json({
							err_code:2,
							message:'permission denied',
						})
					}else{
						//保存
						body.created_time = formTime();
						new Saying(body).save(function (err,user,next) {
							if(err) {
								return next(err)
							}
							return res.status(200).json({
									err_code:3,
									message:'saying post success'
								})
							})
					}
				}
			})//user
		}//else
	})//jwt

})
//语录获取
router.get('/api/manage/saying/get',function(req,res,next){
	const query = req.query;
	query.limit = parseInt(query.limit)
	Saying.estimatedDocumentCount({},function (err,data) {
		if(err) {
			return next(err)
		}
		if(!data){
			return res.status(200).json({
				err_code:0,
				message:'no saying !'
			})
		}else{
			let total =data
			Saying.find({})
				.skip(query.page*query.limit)
				.limit(query.limit)
				.sort({'_id':-1})
				.exec(function (err,data) {
					if(err){
						 next(err)
					}else{
						res.status(200).json({
							err_code:2,
							message:'ok',
							data:data,
							total:total
						})
					}
				})
		}
	})//total
})
//语录删除
router.post('/api/manage/saying/del',function(req,res,next){
	var body = req.body;
	//登陆状态校验
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
    jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
        if (err) {  //  时间失效的时候 || 伪造的token
            res.status(200).json({
				err_code:0,
				message:'token fail'
			})
        } else {
			User.find({email:decode.email},function(err,data){
				if(err){
					next(err)
				}
				//查不到自己的身份，你完了
				if(!data){
					res.status(200).json({
						err_code:1,
						message:'you disappear !'
					})
				}else{
					//权限不足
					if(!data[0].isAdmin){
						res.status(200).json({
							err_code:2,
							message:'permission denied',
						})
					}else{
						//删除
						Saying.deleteOne({_id:body._id},function (err,raw) {
							if(err) {
								return next(err)
							}
							if(raw) {
								res.status(200).json({
									err_code:3,
									message:'OK'
								})
							}
							if(!raw) {
								res.status(200).json({
									err_code:4,
									message:'fail'
								})
							}
						})
					}
				}
			})
        }
    })
})

//语录修改
router.post('/api/manage/saying/edt',function(req,res,next){
	var body = req.body;
	if(body.saying.length < 1){
		return res.status(200).json({
			err_code:4,
			message:'parameter is wrong'
		})
	}
	//登陆状态校验
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
    jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
        if (err) {  //  时间失效的时候 || 伪造的token
            res.status(200).json({
				err_code:0,
				message:'token fail'
			})
        } else {
			User.find({email:decode.email},function(err,data){
				if(err){
					next(err)
				}
				//查不到自己的身份，你完了
				if(!data){
					res.status(200).json({
						err_code:1,
						message:'you disappear !'
					})
				}else{
					//权限不足
					if(!data[0].isAdmin){
						res.status(200).json({
							err_code:2,
							message:'permission denied',
						})
					}else{
						//进行修改
						Saying.updateOne({_id:body._id},{saying:body.saying},{multi:true},function (err,raw) {
							if(err) {
								return next(err)
							}
							if(raw) {
								res.status(200).json({
									err_code:3,
									message:'saying update success'
								})
							}
						})
					}
				}
			})
        }
    })
})

////****************************友链*************************************** */
//友链保存
router.post('/api/manage/link/save',function(req,res,next){
	const body = req.body
	if(body.name == '' || body.link == '' || body.description == ''){
		return res.status(200).json({
			err_code:4,
			message:'params is worning'
		})
	}
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
    jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
        if (err) {  //  时间失效的时候 || 伪造的token
            res.status(200).json({
				err_code:0,
				message:'token fail'
			})
        } else {
			User.find({email:decode.email},function(err,data){
				if(err){
					next(err)
				}
				//查不到自己的身份，你完了
				if(!data){
					res.status(200).json({
						err_code:1,
						message:'you disappear !'
					})
				}else{
					//权限不足
					if(!data[0].isAdmin){
						res.status(200).json({
							err_code:2,
							message:'permission denied',
						})
					}else{
						//保存
						body.created_time = formTime();
						new FriendlyLink(body).save(function (err,user,next) {
							if(err) {
								return next(err)
							}
							return res.status(200).json({
									err_code:3,
									message:'FriendlyLink post success'
								})
							})
					}
				}
			})//user
		}//else
	})//jwt

})
//友链获取
router.get('/api/manage/link/get',function(req,res,next){
	const query = req.query;
	query.limit = parseInt(query.limit)
	FriendlyLink.estimatedDocumentCount({},function (err,data) {
		if(err) {
			return next(err)
		}
		if(!data){
			return res.status(200).json({
				err_code:0,
				message:'no link !'
			})
		}else{
			let total =data
			FriendlyLink.find({})
				.skip(query.page*query.limit)
				.limit(query.limit)
				.sort({'_id':-1})
				.exec(function (err,data) {
					if(err){
						 next(err)
					}else{
						res.status(200).json({
							err_code:1,
							message:'ok',
							data:data,
							total:total
						})
					}
				})
		}
	})//total
})
//友链删除
router.post('/api/manage/link/del',function(req,res,next){
	var body = req.body;
	//登陆状态校验
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
    jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
        if (err) {  //  时间失效的时候 || 伪造的token
            res.status(200).json({
				err_code:0,
				message:'token fail'
			})
        } else {
			User.find({email:decode.email},function(err,data){
				if(err){
					next(err)
				}
				//查不到自己的身份，你完了
				if(!data){
					res.status(200).json({
						err_code:1,
						message:'you disappear !'
					})
				}else{
					//权限不足
					if(!data[0].isAdmin){
						res.status(200).json({
							err_code:2,
							message:'permission denied',
						})
					}else{
						//删除
						FriendlyLink.deleteOne({_id:body._id},function (err,raw) {
							if(err) {
								return next(err)
							}
							if(raw) {
								res.status(200).json({
									err_code:3,
									message:'OK'
								})
							}
							if(!raw) {
								res.status(200).json({
									err_code:4,
									message:'fail'
								})
							}
						})
					}
				}
			})
        }
    })
})

//友链修改
router.post('/api/manage/link/edt',function(req,res,next){
	var body = req.body;
	if(body.name == '' || body.link == '' || body.description == '' || body._id == ''){
		return res.status(200).json({
			err_code:4,
			message:'params is worning'
		})
	}
	//登陆状态校验
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
    jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
        if (err) {  //  时间失效的时候 || 伪造的token
            res.status(200).json({
				err_code:0,
				message:'token fail'
			})
        } else {
			User.find({email:decode.email},function(err,data){
				if(err){
					next(err)
				}
				//查不到自己的身份，你完了
				if(!data){
					res.status(200).json({
						err_code:1,
						message:'you disappear !'
					})
				}else{
					//权限不足
					if(!data[0].isAdmin){
						res.status(200).json({
							err_code:2,
							message:'permission denied',
						})
					}else{
						//进行修改
						FriendlyLink.updateOne({_id:body._id},body,{multi:true},function (err,raw) {
							if(err) {
								return next(err)
							}
							if(raw) {
								res.status(200).json({
									err_code:3,
									message:'FriendlyLink update success'
								})
							}
						})
					}
				}
			})
        }
    })
})


////****************************文章*************************************** */
//文章保存
router.post('/api/manage/article/save',function(req,res,next){
	const body = req.body
	//不选择时间，为现在时间
	if(!body.date || !body.time){
		body.created_time = formTime();
	}else{
		body.created_time = body.date + ' ' + body.time
	}
	for (var k in body) {
		if(k=='date' || k == 'time'){
			delete body[k];
		}
	}
	//检查参数
	if(body.desc.length > 100 || !body.contentTitle || !body.lanmu || !body.classify || !body.content || body.contentTitle.length>30 || body.tag.length>1){
		return res.status(200).json({
			err_code:4,
			message:'params is worning'
		})
	}
	body.tag = body.tag[0]
	let isLogin = checkLoginState(req,res)
	if(isLogin.code == 1){
		//保存
		let decode = isLogin.data
		body.email = decode.email
		var saveTo = path.join(__dirname.replace('router', 'uploads'),"./articles/"+Date.now()+".txt");
		var fileUrl = "/uploads/articles/" + path.parse(saveTo).base
		fs.writeFile(saveTo,body.content,function (err) {
			if(err) {
				return next(err)
			}
			delete body['content']
			body.contentUrl = fileUrl
			let imgUS = body.imgUS
			let videoUS = body.videoUS
			delete body['imgUS']
			delete body['videoUS']
			new Article(body).save(function (err,data) {
				if(err) {
					return next(err)
				}else{
					let ss = {articleid:data._id,imgUS:imgUS,videoUS:videoUS,created_time:new Date().getTime()}
					new Link(ss).save((err)=>{
						if(err) {
							 next(err)
						}else{
							return res.status(200).json({
									err_code:3,
									message:'Article post success'
								})
						}
					})
				}//else
		})//保存				
		})//fs
	}//isLogin.code == 1

})

//文章获取(审核和未审核的)
router.get('/api/manage/article/get',function(req,res,next){
	const query = req.query;
	query.limit = parseInt(query.limit)
	Article.estimatedDocumentCount({},function (err,data) {
		if(err) {
			return next(err)
		}
		if(!data){
			return res.status(200).json({
				err_code:0,
				message:'no link !'
			})
		}else{
			let total =data
			Article.find({})
				.skip(query.page*query.limit)
				.limit(query.limit)
				.sort({'_id':-1})
				.exec(function (err,data) {
					if(err){
						 next(err)
					}else{
						res.status(200).json({
							err_code:1,
							message:'ok',
							data:data,
							total:total
						})
					}
				})
		}
	})//total
})
//文章获取(审核的)
//扩展功能 classify=0(文章类别) tag=0 (标签)
router.get('/api/manage/article/getBy',function(req,res,next){
	const query = req.query;
	let select = {check:1}
	if(query.lanmu){
		select.lanmu = query.lanmu
	}
	if(query.tag){
		select.tag = query.tag
	}
	query.limit = parseInt(query.limit)
	Article.countDocuments(select,function (err,data) {
		if(err) {
			return next(err)
		}
		if(!data){
			return res.status(200).json({
				err_code:0,
				message:'no link !'
			})
		}else{
			let total =data
			Article.find(select)
				.skip(query.page*query.limit)
				.limit(query.limit)
				.sort({'_id':-1})
				.exec(function (err,data) {
					if(err){
						 next(err)
					}else{
						res.status(200).json({
							err_code:1,
							message:'ok',
							data:data,
							total:total
						})
					}
				})
		}
	})//total
})
//文章删除
router.post('/api/manage/article/del',function(req,res,next){
	var body = req.body;
	//登陆状态校验
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
    jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
        if (err) {  //  时间失效的时候 || 伪造的token
            res.status(200).json({
				err_code:0,
				message:'token fail'
			})
        } else {
			User.findOne({email:decode.email},function(err,data){
				if(err){
					next(err)
				}
				//查不到自己的身份，你完了
				if(!data){
					res.status(200).json({
						err_code:1,
						message:'you disappear !'
					})
				}else{
					//权限不足
					if(!data.isAdmin){
						res.status(200).json({
							err_code:2,
							message:'permission denied',
						})
					}else{
						body.contentUrl = body.contentUrl.slice(8)
						var removeUrl = path.join(__dirname.replace('router', 'uploads'),"./" + body.contentUrl);
						//删除文件
						fs.unlink(removeUrl,(err)=>{
							if(err){
								throw err;
							}else{
								//删除数据库
								Article.deleteOne({_id:body._id},function (err,raw) {
									if(err) {
										return next(err)
									}
									if(raw) {
										Comment.deleteMany({article:body._id},function(err,raw){
											if(err) {
												return next(err)
											}else{
											Link.updateOne({articleid:body._id},{isDel:true},{multi:true},(err,data)=>{
												if(err) {
													return next(err)
												}
												res.status(200).json({
													err_code:3,
													message:'OK'
												})
											})
											}
										})
									}
									if(!raw) {
										res.status(200).json({
											err_code:4,
											message:'fail'
										})
									}
								})
							}
						});
					}
				}
			})
        }
    })
})

//id获取单个文章相关信息
router.get('/api/manage/article/info',function(req,res,next){
	let query = req.query
	// console.log(query);
	if(!query._id) return false;
	Article.findOne({_id:query._id},function(err,data){
		if(err){
			next (err)
		}
		if(data){
			Article.updateOne({_id:data._id},{PV:++data.PV},{multi:true},function(err,data){
				if(err){
					next(err)
				}
			})
		}
		res.status(200).json({
			err_code:1,
			message:'ok',
			data:data
		})
	})
})


//url获取文章内容
router.get('/api/manage/article/getByUrl',function(req,res,next){
	let query = req.query;
	query.contentUrl = query.contentUrl.slice(8)
	var readUrl = path.join(__dirname.replace('router', 'uploads'),"./" + query.contentUrl);
	fs.readFile(readUrl,'utf-8',(err,data)=>{
		if(err){
			next(err)
		}else{
			res.status(200).json({
				err_code:0,
				data:data
			})
		}
	})

})

//修改文章
router.post('/api/manage/article/edt',function(req,res,next){
	var body = req.body;
	if(body.contentTitle == '' || body.lanmu == '' || body.classify == '' || body._id == '' || body.contentTitle.length>30 || body.content == '' || body.contentUrl == ''){
		return res.status(200).json({
			err_code:4,
			message:'params is worning'
		})
	}
	//登陆状态校验
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
    jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
        if (err) {  //  时间失效的时候 || 伪造的token
            res.status(200).json({
				err_code:0,
				message:'token fail'
			})
        } else {
			User.find({email:decode.email},function(err,data){
				if(err){
					next(err)
				}
				//查不到自己的身份，你完了
				if(!data){
					res.status(200).json({
						err_code:1,
						message:'you disappear !'
					})
				}else{
					//权限不足
					if(!data[0].isAdmin){
						res.status(200).json({
							err_code:2,
							message:'permission denied',
						})
					}else{
						//进行修改
						body.date  = moment(body.date).format('YYYY-MM-DD HH:mm:ss.SSS');
						body.created_time = body.date.slice(0,10) + " " + body.time
						body.contentUrl = body.contentUrl.slice(8)
						var saveUrl = path.join(__dirname.replace('router', 'uploads'),"./" + body.contentUrl);
						let id = body._id;
						for (var k in body) {
							if(k=='date' || k == 'time' || k == 'contentUrl' || k == '_id'){
								delete body[k];
							}
						}
						fs.writeFile(saveUrl,body.content,function (err) {
							if(err) {
								return next(err)
							}else{
								delete body['content']
								Article.updateOne({_id:id},body,{multi:true},function (err,raw) {
									if(err) {
										return next(err)
									}
									if(raw) {
										res.status(200).json({
											err_code:3,
											message:'FriendlyLink update success'
										})
									}
								})
							}
						})
					}
				}
			})
        }
    })
})

//文章审核变更
router.post('/api/manage/article/checkToChange',function(req,res){
	let body = req.body
	//登陆状态校验
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
    jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
        if (err) {  //  时间失效的时候 || 伪造的token
            res.status(200).json({
				err_code:0,
				message:'token fail'
			})
        } else {
			User.find({email:decode.email},function(err,data){
				if(err){
					next(err)
				}
				//查不到自己的身份，你完了
				if(!data){
					res.status(200).json({
						err_code:1,
						message:'you disappear !'
					})
				}else{
					//权限不足
					if(!data[0].isAdmin){
						res.status(200).json({
							err_code:2,
							message:'permission denied',
						})
					}else{
						//进行修改
						Article.findById({_id:body._id},function(err,data){
							if(err) {
								return next(err)
							}else{
								let check = data.check;
								if(check){
									check=0
								}else{
									check=1
								}
								Article.updateOne({_id:body._id},{check:check},{multi:true},function (err,raw) {
									if(err) {
										return next(err)
									}
									if(raw) {
										res.status(200).json({
											err_code:3,
											message:'FriendlyLink update success'
										})
									}
								})
							}
						})
					}
				}
			})
		}
	})
})


////****************************页面*************************************** */

//页面的座右铭
router.get('/api/motto',function(req,res){
	Saying.find({}).sort({"_id":1}).limit(1).exec(function(err,data){
		if(err){
			next(err)
		}
		res.status(200).json({
			err_code:0,
			message:'ok',
			data:data
		})
	})
})
//页面的热门文章
router.get('/api/hot',function(req,res){
	Article.find({check:1},{contentTitle:1,PV:1}).sort({"PV":-1}).limit(3).exec(function(err,data){
		if(err){
			next(err)
		}
		res.status(200).json({
			err_code:0,
			message:'ok',
			data:data
		})
	})
})
//评论统计
router.get('/api/comment/total',function(req,res){
	Article.find({},{comment:1,_id:0},function (err,data) {
		if(err){
			next(err)
		}
		let total = 0;
		data.forEach(item=>{
			total = total + item.comment
		})
		res.status(200).json({
			err_code:0,
			message:'ok',
			data:total
		})
	})
})

//主页访问量统计 
//每天一个ip为访问1
router.get('/api/ip',function(req,res){
	let getClientIp = function (req) {
		return req.headers['x-forwarded-for'] ||
			req.connection.remoteAddress ||
			req.socket.remoteAddress ||
			req.connection.socket.remoteAddress || '';
	};
	let ip = getClientIp(req).match(/\d+.\d+.\d+.\d+/);
	let time =new Date().getTime()
	ip = ip ? ip.join('.') : null;
	if(ip){
		Ip.findOne({ip:ip},function(err,data){
			if(err){
				next(err)
			}
			if(data){
				//ip存在数据库不用保存，判断时间是否超过一天，是visit+1，
				data.time = parseInt(parseInt(data.time) /1000)
				if(parseInt(time / 1000) - data.time >= 86400){
					Ip.updateOne({_id:data._id},{time:time},{multi:true},function(err,data){
						if(err){
							next(err)
						}
					})
					visitRW(res);
				}else{
					res.status(200).json({
						code:0,
						message:'no one day',
					})
				}

			}else{
				//ip不存在数据库中，保存 visit+1
				new Ip({ip:ip,time:time}).save(function(err,data){
					if(err){
						next(err)
					}
					visitRW(res)
				})
			}
		})
	}else{
		res.status(200).json({
			code:2,
			message:'ip is empty',
		})
	}
})

//获取总访问量
router.get('/api/visitTotal',function(req,res){
	let url =path.join(__dirname.replace('router', 'models'),"./" + 'visit.json')
		fs.readFile(url,'utf-8',function(err,data){
			if(err){
				next(err)
			}
			data = JSON.parse(data)
			data.visit = parseInt(data.visit)
			data.time = new Date()	
			res.status(200).json({
				code:0,
				message:'total visit',
				data:data
			})

		})
})

//文章评论保存
router.post('/api/comment/save',function(req,res){
	let body = req.body
	if(!body.comment || !body.id || body.comment.length > 300){
		return res.status(200).json({
			err_code:4,
			message:'params is worning'
		})
	}
	let isLogin = checkLoginState(req,res)
	if(isLogin.code == 1){
		let sa = {email:isLogin.data.email,created_time:formTime(),comment:body.comment,article:body.id}
		new Comment(sa).save(function(err,data){
			if(err){
				next(err)
			}
			if(data){
				Article.updateOne({_id:body.id},{$inc:{comment:1}},{multi:true},function(err,data){
					if(err){
						next(err)
					}
					res.status(200).json({
						err_code:1,
						message:'ok'
					})
				})
			}			
		})
	}
	
})
//文章评论获取
router.get('/api/comment/get',function(req,res){
	let query = req.query
	Comment.countDocuments({article:query.id},function(err,data){
		if(err){
			next(err)
		}
		let total = data
		Comment.find({article:query.id})
			.sort({'_id':-1})
			.skip(query.page*5)
			.limit(5)
			.exec(function(err,data){
				if(err){
					next(err)
				}
				res.status(200).json({
					err_code:1,
					message:'ok',
					data:data,
					total:total
				})
	
			})
	})
})
//文章评论删除
router.post('/api/comment/del',function(req,res){
	let body = req.body
	let isLogin = checkLoginState(req,res)
	if(isLogin.code == 1){
		User.findOne({email:isLogin.data.email},function(err,data){
			if(err){
				next(err)
			}
			if(data.isAdmin){
				Comment.deleteOne({_id:body.id},function(err,data){
					if(err){
						next(err)
					}
					if(data){
						Article.updateOne({_id:body.articleId},{$inc:{comment: - 1}},{multi:true},function(err,data){
							if(err){
								next(err)
							}
							res.status(200).json({
								err_code:1,
								message:'ok'
							})
						})
					}
				})
				//不是管理员只能删除自己的评论
			}else{
				Comment.findOne({_id:body.id},function(err,data){
					if(err){
						next(err)
					}
					if(data.email == isLogin.data.email){
						Comment.deleteOne({_id:body.id},function(err,data){
							if(err){
								next(err)
							}
							Article.updateOne({_id:body.articleId},{$inc:{comment: -1}},{multi:true},function(err,data){
								if(err){
									next(err)
								}
								res.status(200).json({
									err_code:1,
									message:'ok'
								})
							})
						})
					}else{
						res.status(200).json({
							err_code:2,
							message:'fail',
						})
					}
					
				})
			}
		})
	}
	
})

//模糊查询
router.get('/api/article/search',function(req,res,next){
	let query = req.query
	query.page = parseInt(query.page)
	query.limit = parseInt(query.limit)
	if(!query.searchName){
		return res.status(200).json({
			err_code:4,
			message:'params is worning'
		})
	}
	const reg = new RegExp(query.searchName, 'i')
	let select = {check:1,contentTitle:{ $regex:reg }}
	Article.countDocuments(select,function (err,data) {
		if(err) {
			return next(err)
		}
		if(!data){
			return res.status(200).json({
				err_code:0,
				message:'no link !'
			})
		}else{
			let total =data
			Article.find(select)
				.skip(query.page*query.limit)
				.limit(query.limit)
				.sort({'_id':-1})
				.exec(function (err,data) {
					if(err){
						 next(err)
					}else{
						res.status(200).json({
							err_code:1,
							message:'ok',
							data:data,
							total:total
						})
					}
				})
		}
	})//total
})

//上传凭证获取
router.get('/api/uploadToken',(req,res,next)=>{
	let isLogin = checkLoginState(req,res)
	if(isLogin.code == 1){
		let accessKey = 'ReTyr9f4ooP5QyfPjqF9m4KG9BFA05oyI_GROX7M';
		let secretKey = 'OBOY0-oXBxgr_nuEf9FRGlT1zKJVtLN_A-Xfxa-q';
		let bucket = 'happychuan'
		let mac = new qiniu.auth.digest.Mac(accessKey, secretKey);
		let options = {
			scope: bucket,
		};
		let putPolicy = new qiniu.rs.PutPolicy(options);
		let uploadToken=putPolicy.uploadToken(mac);
		res.end(JSON.stringify({code:1,token:uploadToken}))
	}
})

//清理七牛云的垃圾
router.get('/api/clear',(req,res,next)=>{
	let accessKey = 'ReTyr9f4ooP5QyfPjqF9m4KG9BFA05oyI_GROX7M';
	let secretKey = 'OBOY0-oXBxgr_nuEf9FRGlT1zKJVtLN_A-Xfxa-q';
	qiniu.conf.ACCESS_KEY = 'ReTyr9f4ooP5QyfPjqF9m4KG9BFA05oyI_GROX7M';
	qiniu.conf.SECRET_KEY = 'OBOY0-oXBxgr_nuEf9FRGlT1zKJVtLN_A-Xfxa-q';
	let mac = new qiniu.auth.digest.Mac(accessKey, secretKey);
	let config = new qiniu.conf.Config();
	config.zone = qiniu.zone.Zone_z2;
	let bucketManager = new qiniu.rs.BucketManager(mac, config);
	run()
	async function run(){
		let begin = new Date()	
		let a =  await getKey()
		let end = new Date()
		let keyArr  = await  Link.deleteMany({isDel:true},(err,data)=>{
			if(err){
				return next(err)
			}
			return res.status(200).json({
				err_code:1,
				message:`用时${end - begin} 毫秒`,
			})
		})
	}
	//删除文章外链key
	async function getKey(){
		let keyArr  = await  Link.find({isDel:true},{imgUS:1,videoUS:1,_id:0},(err,data)=>{
			if(err){
				return next(err)
			}
		})
		let imgArr = []
		let videoArr = []
		let srcBucket = 'happychuan'
		keyArr.forEach(item=>{
			imgArr = imgArr.concat(item.imgUS)
			videoArr = videoArr.concat(item.videoUS)
		})
		imgArr = unique(imgArr)
		videoArr = unique(videoArr)
		imagenum = imgArr.length
		videonum = videoArr.length
		//七牛云操作
		let delOptn = []
		imgArr.forEach(item=>{
			delOptn.push(qiniu.rs.deleteOp(srcBucket,item))
		})
		let delOptn1 = []
		videoArr.forEach(item=>{
			delOptn1.push(qiniu.rs.deleteOp(srcBucket,item))
		})
		delobj(delOptn)
		delobj(delOptn1)
		function delobj(obj){
		  bucketManager.batch(obj, function(err, respBody, respInfo) {
			if (err) {
			  console.log(err);
			  //throw err;
			} else {
			  // 200 is success, 298 is part success
			  if (parseInt(respInfo.statusCode / 100) == 2) {
				respBody.forEach(function(item) {
				  if (item.code == 200) {
					// console.log(item.code + "\tsuccess");
				  } else {
					// console.log(item.code + "\t" + item.data.error);
				  }
				});
			  } else {
				// console.log(respInfo.deleteusCode);
				// console.log(respBody);
			  }
			}
		  });
		
	}
}

})

//时间格式
function formTime(){
    let time =new Date()
    let year = time.getFullYear()
    let month = (time.getMonth()+1).toString().padStart(2,0)
    let day = time.getDate().toString().padStart(2,0)
    let hour = time.getHours().toString().padStart(2,0)
    let minutes = time.getMinutes().toString().padStart(2,0)
    let second = time.getSeconds().toString().padStart(2,0)
    return `${year}-${month}-${day} ${hour}:${minutes}:${second}`;
}
//数组去重
function unique (arr) {
	return Array.from(new Set(arr))
}



//标签总数
// function totalTag(){
// 	return new Promise((resolve, reject)=>{
// 		Tag.estimatedDocumentCount({},function (err,data) {
// 			if(err){
// 				reject(err)
// 			}
// 			resolve(data)
// 		})
// 	})
// }
//需保存标签
// function needSaveTag(tag){
// 	return new Promise((resolve,reject)=>{
// 		Tag.find({},function(err,data){
// 			if(err){
// 				reject(err)
// 			}
// 			let dataName = [];
// 			if(data.length < 1){
// 				data=[{name:'无'},]
// 			}
// 			for(let i=0;i<data.length;i++){
// 				dataName[i] = data[i].name
// 			}
// 			//dataName 数据库中含有的tag
// 			//共同所有标签
// 			let commonTag = unique(tag.concat(dataName).filter(item => (tag.includes(item)&&dataName.includes(item))))//共同所有
// 			//非共同所有标签(要保存的标签)
// 			let que = tag.concat(commonTag).filter(item => !(tag.includes(item)&&commonTag.includes(item))) //新的tag
// 			//不要保存的标签
// 			let noSave = unique(tag.concat(commonTag).filter(item => (tag.includes(item)&&commonTag.includes(item))))
			
// 			// let noSaveIndex = [] //不要保存的标签的索引
// 			// if(noSave.length > 0){
// 			// 	for(let i=0;i<noSave.length;i++){
// 			// 		noSaveIndex[i] = dataName.indexOf(noSave[i]) + 1
// 			// 	}
// 			// }
// 			let obj = {code:0}
// 			//没有共同tag保存到数据库 返回id
// 			if(commonTag.length < 1){
// 				obj.id = tag[0]
// 			}
// 			//有共同tag 返回id
// 			if(commonTag.length >= 1){
// 				for(let i =0;i<data.length;i++){
// 					if(data[i].name.toUpperCase() == commonTag[0].toUpperCase()){
// 						 obj.id = data[i]._id;
// 						 obj.code = 1;
// 						 break;
// 					}
// 				}
// 			}
// 			resolve(obj)
// 		})//find
// 	})
// }
// //标签保存
// function sv(yy){
// 	return new Promise((resolve,reject)=>{
// 		new Tag(yy).save(function(err,data){
// 			if(err){
// 				reject(err)
// 			}
// 			resolve (data._id)
// 		})
// 	})
// }
// //标签
// async function tagAction(tag){
// 	// let total =  await totalTag()
// 	if(tag.length > 0){
// 		let saveTag = await needSaveTag(tag)
// 		// console.log(saveTag);
// 		if(saveTag.code == 1){
// 			return saveTag.id
// 		}
	
// 		let vv = await sv({name:saveTag.id})
// 		return vv;
// 	}else{
// 		return '0';
// 	}
// 	// new Tag({name:saveTag.id}).save(function(err,data){
// 	// 	if(err){
// 	// 		next(err)
// 	// 	}
// 	// 	return data._id
// 	// })
// 	// return;
// 	//saveTag[0]   需要保存的标签名字
// 	//saveTag[1]   不需要保存的标签的索引
// 	// if(saveTag[0].length > 0){
// 	// 	for(let i = 0;i<saveTag[0].length;i++){
// 	// 		data.push({name:saveTag[0][i],id:++total})
// 	// 	}
// 	// 	var res = await Tag.insertMany(data)
// 	// 	if(res){
// 	// 		for(let i =0;i<res.length;i++){
// 	// 			saveTag[1].push(res[i].id)
// 	// 		}
// 	// 		//保存标签，返回索引
// 	// 		return saveTag[1];
// 	// 	}
// 	// }else{
// 	// 	//没有要保存的标签，返回索引
// 	// 	return saveTag[1];
// 	// }
// }
//检查登陆状态


function checkLoginState(req,res){
	let token = req.get("Authorization"); //获取请求头的信息
	let secretOrPrivateKey="happychuan"; // 这是加密的key（密钥）
	let code = {code:0}
	jwt.verify(token, secretOrPrivateKey, (err, decode)=> {
		if (err) {  //  时间失效的时候 || 伪造的token
			res.status(200).json({
				err_code:0,
				message:'token fail'
			})
		} else {
			code.code = 1;
			code.data = decode;
		}
	})
	return code;
}

//访问量读取与写入
function visitRW(res){
		let url =path.join(__dirname.replace('router', 'models'),"./" + 'visit.json')
		fs.readFile(url,'utf-8',function(err,data){
			if(err){
				next(err)
			}
			data = JSON.parse(data)
			data.visit = parseInt(data.visit)
			data.time = new Date()	
			data.visit = data.visit + 1
			let vv = data
			fs.writeFile(url,JSON.stringify(data).toString(),'utf-8',function(err,data){
				if(err){
					next(err)
				}
				res.status(200).json({
					code:1,
					message:'ok',
					data:vv
				})
			})	
	})		
}


module.exports = router