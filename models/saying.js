const mongoose = require('./connect')
const Schema = mongoose.Schema
var sayingSchema = new Schema({
    saying:{
        type:String,
        required:true,
    },
    created_time:{
        type:String,
        default:formTime()
    },


})
function formTime(){
    let time =new Date()
    let year = time.getFullYear()
    let month = (time.getMonth()+1).toString().padStart(2,0)
    let day = time.getDate().toString().padStart(2,0)
    let hour = time.getHours().toString().padStart(2,0)
    let minutes = time.getMinutes().toString().padStart(2,0)
    let second = time.getSeconds().toString().padStart(2,0)
    return `${year}-${month}-${day} ${hour}:${minutes}:${second}`;
}

module.exports = mongoose.model('Saying',sayingSchema)