const mongoose = require('./connect')
const Schema = mongoose.Schema
var userSchema = new Schema({
    email:{
        type: String,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    name:{
        type:String,
        required:true
    },
    isAdmin:{
        type:Boolean,
        required:true,
        default:false,
    },
    avatar:{
        type:String,
        default:'/assets/default-avatar.png'
    },
    created_time:{
        type:String,
        default:formTime()
    },

})
function formTime(){
    let time =new Date()
    let year = time.getFullYear()
    let month = (time.getMonth()+1).toString().padStart(2,0)
    let day = time.getDate().toString().padStart(2,0)
    let hour = time.getHours().toString().padStart(2,0)
    let minutes = time.getMinutes().toString().padStart(2,0)
    let second = time.getSeconds().toString().padStart(2,0)
    return `${year}-${month}-${day} ${hour}:${minutes}:${second}`;
}


module.exports = mongoose.model('User',userSchema)