const mongoose = require('./connect')
const Schema = mongoose.Schema
var userSchema = new Schema({
    email:{
        type:String,
        required:true
    },
    created_time:{
        type:String,
        default:currentTime()
    },
    contentTitle:{
        type:String,
        default:'',
        required:true
    },
    contentUrl:{
        type:String,
        default:'',
        required:true
    },
    classify:{
        type:Number,
        enum: [0,1,2],
        default:0
    },
    like:{
        type:Number,
        default:0
    },
    unlike:{
        type:Number,
        default:0
    },
    PV:{
        type:Number,
        default:0
    },
    tag:{
        type:String,
        default:'无'
    },
    check:{
        type:Number,
        enum: [0,1,2],
        default:0
    },
    lanmu:{
        type:Number,
        enum: [0,1],
        default:0
    },
    desc:{
        type:String,
        default:'',
    },
    comment:{
        type:Number,
        default:0
    },
    cover:{
        type:String,
        default:'https://happychuan.oss-cn-shenzhen.aliyuncs.com/default/default-cover.jpg'
    }


})

function currentTime(){
    return (new Date()).getTime().toString();
}

module.exports = mongoose.model('Article',userSchema)