const mongoose = require('./connect')
const Schema = mongoose.Schema
var linkSchema = new Schema({
    articleid:{
        type:String,
        required:true
    },
    imgUS:{
        type:Array,
        required:true
    },
    videoUS:{
        type:Array,
        required:true
    },
    created_time:{
        type:String,
        default:currentTime()
    },
    isDel:{
        type:Boolean,
        default:false
    }
})

function currentTime(){
    return (new Date()).getTime().toString();
}

module.exports = mongoose.model('Link',linkSchema)