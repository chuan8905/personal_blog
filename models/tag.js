const mongoose = require('./connect')
const Schema = mongoose.Schema
var tagSchema = new Schema({
    name:{
        type:String,
        required:true,
        default:'无'
    },
})

module.exports = mongoose.model('Tag',tagSchema)