const mongoose = require('./connect')
const Schema = mongoose.Schema
var ipSchema = new Schema({
    ip:{
        type:String,
        required:true
    },
    time:{
        type:String,
        default:new Date().getTime()
    }

})

module.exports = mongoose.model('Ip',ipSchema)